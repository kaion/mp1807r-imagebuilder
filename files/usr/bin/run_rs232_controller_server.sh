#!/bin/sh
# File              : run_rs232_controller_server.sh
# Author            : Shihyu <shihyu@program.com.tw>
# Date              : 2018-12-22
# Last Modified Date: 2018-12-22
# Last Modified By  : Shihyu <shihyu@program.com.tw>
while true
do
	MPRunRS232ControllerServer >/dev/null 2>&1
	sleep 5
done

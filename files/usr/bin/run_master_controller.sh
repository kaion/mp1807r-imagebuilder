#!/bin/sh
# File              : run_master_controller_.sh
# Author            : Shihyu <shihyu@program.com.tw>
# Date              : 2019-02-18
# Last Modified Date: 2019-02-18
# Last Modified By  : Shihyu <shihyu@program.com.tw>

while true
do
	MPRunMasterController >/dev/null 2>&1
	sleep 5
done

#!/bin/sh
# File              : run_controller.sh
# Author            : Shihyu <shihyu@program.com.tw>
# Date              : 2019-02-25
# Last Modified Date: 2019-02-25
# Last Modified By  : Shihyu <shihyu@program.com.tw>

while true
do
	MPRunController >/dev/null 2>&1
	sleep 5
done

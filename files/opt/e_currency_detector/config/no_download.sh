tmp_upgrade_time_dir=/opt/e_currency_detector/tmp
tmp_upgrade_time=/opt/e_currency_detector/tmp/upgrade_time
tmp_error_upgrade_time=/opt/e_currency_detector/tmp/error_upgrade_time

if [ ! -d $tmp_upgrade_time_dir ]; then
    mkdir $tmp_upgrade_time_dir
fi

if [ ! -f $tmp_upgrade_time ]; then
    touch $tmp_upgrade_time
fi

if [ -e $tmp_error_upgrade_time ]; then
    rm $tmp_error_upgrade_time
fi

date +"%Y-%m-%d %H:%M:%S" > $tmp_upgrade_time

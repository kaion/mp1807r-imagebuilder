tmp_upgrade_time_dir=/opt/e_currency_detector/tmp
tmp_error_upgrade_time=/opt/e_currency_detector/tmp/error_upgrade_time

if [ ! -d $tmp_upgrade_time_dir ]; then
    mkdir $tmp_upgrade_time_dir
fi

if [ ! -f $tmp_error_upgrade_time ]; then
    touch $tmp_error_upgrade_time
fi

date +"%Y-%m-%d %H:%M:%S" > $tmp_error_upgrade_time

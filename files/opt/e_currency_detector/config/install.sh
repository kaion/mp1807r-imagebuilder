tmp_upgrade_time_dir=/opt/e_currency_detector/tmp
tmp_upgrade_time=/opt/e_currency_detector/tmp/upgrade_time
tmp_error_upgrade_time=/opt/e_currency_detector/tmp/error_upgrade_time

/etc/init.d/mp_controller stop

logger MP1807RView-set-static "Show 'system install' on OLED."
MP1807RView-set-static tran -t "安裝更新"

logger easy_install-3 "Install certifi"
easy_install-3 /opt/e_currency_detector/dist/certifi-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install urllib3"
easy_install-3 /opt/e_currency_detector/dist/urllib3-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install idna"
easy_install-3 /opt/e_currency_detector/dist/idna-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install chardet"
easy_install-3 /opt/e_currency_detector/dist/chardet-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install requests"
easy_install-3 /opt/e_currency_detector/dist/requests-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install pyserial"
easy_install-3 /opt/e_currency_detector/dist/pyserial-*.tar.gz 2>&1 | logger

logger easy_install-3 "Install MPgpio"
easy_install-3 /opt/e_currency_detector/dist/MPgpio-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install MPCPSClient"
easy_install-3 /opt/e_currency_detector/dist/MPCPSClient-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install MP1807RView"
easy_install-3 /opt/e_currency_detector/dist/MP1807RView-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install MPECCAPI"
easy_install-3 /opt/e_currency_detector/dist/MPECCAPI-*.tar.gz 2>&1 | logger
logger easy_install-3 "Install MPController"
easy_install-3 /opt/e_currency_detector/dist/MPController-*.tar.gz 2>&1 | logger

if [ ! -d $tmp_upgrade_time_dir ]; then
    mkdir $tmp_upgrade_time_dir
fi

if [ ! -f $tmp_upgrade_time ]; then
    touch $tmp_upgrade_time
fi

if [ -e $tmp_error_upgrade_time ]; then
    rm $tmp_error_upgrade_time
fi

date +"%Y-%m-%d %H:%M:%S" > $tmp_upgrade_time

/etc/init.d/mp_controller start

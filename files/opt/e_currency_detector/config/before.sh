tmp_upgrade_time_dir=/opt/e_currency_detector/tmp

if [ ! -d $tmp_upgrade_time_dir ]; then
    mkdir $tmp_upgrade_time_dir
fi

logger MP1807RView-set-static "Show 'system upgrade' on OLED."
MP1807RView-set-static tran -t "系統更新"

#!/bin/sh
PRODUCT="MP1807R Rev 0.2"
DTS_NAME="mp1807r-rev0.2"

OPENWRT_VER="openwrt-18.06.2"
BUILD_TOOL="../openwrt-imagebuilder-18.06.2-ramips-mt76x8.Linux-x86_64"
LINUX_VERSION="linux-4.14.95"

PACKAGES="-kmod-mt76 kmod-mt7603 $PACKAGES" #WIFI
PACKAGES="kmod-i2c-core kmod-i2c-mt7628 $PACKAGES" #I2C
PACKAGES="kmod-sdhci-mt7620 $PACKAGES" #SD
PACKAGES="kmod-usb-acm kmod-usb-core kmod-usb2 kmod-usb-ohci $PACKAGES" #USB 
PACKAGES="kmod-fs-ext4 kmod-fs-vfat $PACKAGES" #File System (ext4&fat)
PACKAGES="mount-utils block-mount e2fsprogs fdisk blkid usbutils $PACKAGES"
PACKAGES="luci -uboot-envtools $PACKAGES" #kmod-spi-dev libmraa kmod-pwm-mediatek-ramips kmod-usb-storage 
#PACKAGES="-ip6tables -kmod-ip6tables -kmod-ipv6 -kmod-nf-conntrack6 -kmod-nf-ipt6 -libip6tc -luci-proto-ipv6 -odhcpd-ipv6only -odhcp6c $PACKAGES"
PACKAGES="python3 python3-setuptools python3-smbus libstdcpp wget curl rsync tree libustream-openssl $PACKAGES"

NDATE=`date '+%y%m%d-%H%M'`
TITLE="$PRODUCT ($NDATE)"

PROFILE="LinkIt7688"
DIRECTORY=`pwd`

cd $BUILD_TOOL

#sed -i 's#microprogram,mp1807r \##g' target/linux/ramips/image/mt76x8.mk
sed -i 's#linkits7688\ linkits7688d#microprogram,mp1807r#g' target/linux/ramips/image/mt76x8.mk
sed -i 's#MediaTek\ LinkIt\ Smart\ 7688#Microprogram\ MP1807R#g' target/linux/ramips/image/mt76x8.mk
if [ $? -ne 0 ]; then exit 0; fi
cp build_dir/target-mipsel_24kc_musl/linux-ramips_mt76x8/vmlinux $DIRECTORY/vmlinux
if [ $? -ne 0 ]; then exit 0; fi
./build_dir/target-mipsel_24kc_musl/linux-ramips_mt76x8/$LINUX_VERSION/scripts/dtc/dtc -O dtb -o $DIRECTORY/$DTS_NAME.dtb $DIRECTORY/$DTS_NAME.dts
if [ $? -ne 0 ]; then exit 0; fi
./staging_dir/host/bin/patch-dtb $DIRECTORY/vmlinux $DIRECTORY/$DTS_NAME.dtb
if [ $? -ne 0 ]; then exit 0; fi
rm -rf $DIRECTORY/$DTS_NAME.dtb
./staging_dir/host/bin/lzma e $DIRECTORY/vmlinux -lc1 -lp2 -pb2 $DIRECTORY/vmlinux.lzma
if [ $? -ne 0 ]; then exit 0; fi
rm -rf $DIRECTORY/vmlinux
./staging_dir/host/bin/mkimage -A mips -O linux -T kernel -C lzma -a 0x80000000 -e 0x80000000 -n "$TITLE" -d $DIRECTORY/vmlinux.lzma $DIRECTORY/vmlinux.uImage
if [ $? -ne 0 ]; then exit 0; fi
rm -rf $DIRECTORY/vmlinux.lzma
mv $DIRECTORY/vmlinux.uImage ./build_dir/target-mipsel_24kc_musl/linux-ramips_mt76x8/$PROFILE-kernel.bin
make V=s image PROFILE=$PROFILE PACKAGES="$PACKAGES" FILES="$DIRECTORY/files/" #FILES_REMOVE="$DIRECTORY/files_remove"
if [ $? -ne 0 ]; then exit 0; fi
rm -rf $DIRECTORY/vmlinux.uImage
cp bin/targets/ramips/mt76x8/$OPENWRT_VER-ramips-mt76x8-$PROFILE-squashfs-sysupgrade.bin $DIRECTORY/$OPENWRT_VER-ramips-mt76x8-$DTS_NAME-$NDATE-squashfs-sysupgrade.bin
cd $DIRECTORY/
dd if=/dev/zero ibs=1k count=192 | tr "\000" "\377" > $DTS_NAME-spiflash-id1-$NDATE.bin
dd if=uboot.bin of=$DTS_NAME-spiflash-id1-$NDATE.bin conv=notrunc
cat uboot_env.bin >> $DTS_NAME-spiflash-id1-$NDATE.bin
cat factory.bin >> $DTS_NAME-spiflash-id1-$NDATE.bin
cat $OPENWRT_VER-ramips-mt76x8-$DTS_NAME-$NDATE-squashfs-sysupgrade.bin >> $DTS_NAME-spiflash-id1-$NDATE.bin
